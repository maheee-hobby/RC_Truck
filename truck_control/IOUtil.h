#ifndef io_util_h
#define io_util_h


#define setOutput(pin) pinMode(pin, OUTPUT);
#define setInput(pin)  pinMode(pin, INPUT);


namespace IOUtil {

  /*
   * Setup
   */
  inline void setupTimer1() {
    // COM1A1, COM1A0, COM1B1, COM1B0, -, -, WGM11, WGM10
    // Sets "Set OC1 on compare match" and "PWM operation disabled"
    TCCR1A = 0b00000000; 
  
    // ICNC1, ICES1, -, WGM13, WGM12, CS12, CS11, CS10
    TCCR1B = 0b00001001; // 0,0,1 - divide clock by 1 (no prescaling)
  
    // counter
    TCNT1 = 0;
  
    // Interrupt aktivieren
    // -, -, ICIE1, -, -, OCIE1B, OCIE1A, TOIE1
    TIMSK1 = 0b00000001;
  }

  /*
   * Write
   */
  inline void digitalWriteFast(uint8_t port, uint8_t bit, int val) {
    volatile uint8_t *out;
    out = portOutputRegister(port);
   
    if (val == LOW) {
      *out &= ~bit;
    } else {
      *out |= bit;
    }
  }

  inline void digitalWriteFast(uint8_t pin, int val) {
    uint8_t port = digitalPinToPort(pin);
    uint8_t bit = digitalPinToBitMask(pin);
  
    digitalWriteFast(port, bit, val);
  }

  /*
   * Read
   */
  inline int digitalReadFast(uint8_t port, uint8_t bit) {
    if (*portInputRegister(port) & bit) {
      return HIGH;
    }
    return LOW;
  }

  inline int digitalReadFast(uint8_t pin) {
    uint8_t port = digitalPinToPort(pin);
    uint8_t bit = digitalPinToBitMask(pin);
  
    return digitalReadFast(port, bit);
  }

}

#endif
