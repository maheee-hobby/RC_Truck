#ifndef Timer2MicroCounter_h
#define Timer2MicroCounter_h


class Timer2MicroCounter
{
  public:
    Timer2MicroCounter();

    void setup(); 
    void reset();
    
    unsigned long micros();
    unsigned long halfMicros();

  private:
    unsigned long overflow_count;
    unsigned long total_count;

};


#endif
