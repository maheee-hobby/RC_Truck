#include <Adafruit_NeoPixel.h>

#include "IOUtil.h"
#include "PwmReader.h"
#include "Toggler.h"
#include "ShiftRegisterWriter.h"

#define PIN_MOTOR_SHIFT_STROBE  11
#define PIN_MOTOR_SHIFT_DATA    12
#define PIN_MOTOR_SHIFT_CLOCK   13

#define PIN_MOTOR_STEP_0  A0
#define PIN_MOTOR_STEP_1  A1
#define PIN_MOTOR_STEP_2  A2
#define PIN_MOTOR_STEP_3  A3

#define PIN_NEO_PIXEL A5

#define PIN_PULSE_OUTPUT_TEST A4

#define PIN_SIN_0  2
#define PIN_SIN_1  3
#define PIN_SIN_2  4
#define PIN_SIN_3  5
#define PIN_SIN_4  6
#define PIN_SIN_5  7
#define PIN_SIN_6  8
#define PIN_SIN_7  9


uint8_t pwmPins[] = {PIN_SIN_0, PIN_SIN_1, PIN_SIN_2, PIN_SIN_3};

Adafruit_NeoPixel* pixels;
PwmReader *pwmReader;
ShiftRegisterWriter *shiftRegisterWriter;
Toggler* toggler1;
Toggler* toggler2;

void setup() {
  setOutput(PIN_MOTOR_SHIFT_STROBE); setOutput(PIN_MOTOR_SHIFT_DATA); setOutput(PIN_MOTOR_SHIFT_CLOCK);
  
  setOutput(PIN_MOTOR_STEP_0); setOutput(PIN_MOTOR_STEP_1);
  setOutput(PIN_MOTOR_STEP_2); setOutput(PIN_MOTOR_STEP_3);
  
  setOutput(PIN_NEO_PIXEL);

  setOutput(PIN_PULSE_OUTPUT_TEST);
  
  setInput(PIN_SIN_0); setInput(PIN_SIN_1); setInput(PIN_SIN_2); setInput(PIN_SIN_3);
  setInput(PIN_SIN_4); setInput(PIN_SIN_5); setInput(PIN_SIN_6); setInput(PIN_SIN_7);

  Serial.begin(115200);

  IOUtil::setupTimer1();

  toggler1 = new Toggler(100);
  toggler2 = new Toggler(5);

  pixels = new Adafruit_NeoPixel(1, PIN_NEO_PIXEL);
  pixels->begin();

  pwmReader = new PwmReader(digitalPinToPort(pwmPins[0]), pwmPins, 4, 2500);

  shiftRegisterWriter = new ShiftRegisterWriter(PIN_MOTOR_SHIFT_STROBE, PIN_MOTOR_SHIFT_DATA, PIN_MOTOR_SHIFT_CLOCK);
  shiftRegisterWriter->forceWrite();

  attachInterrupt(digitalPinToInterrupt(pwmPins[0]), pwmRising, RISING);
}

ISR(TIMER1_OVF_vect) {
  TCNT1 = 50000; // between 0 and 65.535

  if (toggler2->didToggle()) {
    digitalWrite(PIN_PULSE_OUTPUT_TEST, toggler2->getCurrentState());
  }
}

int restrict(int value, int min, int max) {
  if (value < min) return min;
  if (value > max) return max;
  return value;
}

void pwmRising() {
  pwmReader->updateFromInterrupt();
  //Serial.println(pwmReader->values[0]);
}


void loop() {
  toggler1->setLengthMillis(restrict( (pwmReader->values[0] - 1000) + 10, 10, 2000));
  toggler1->didToggle();

  //int r = restrict((pwmReader->values[0] - 1000) * 255.0d / 1000.0d, 0, 255);
  //int g = restrict((pwmReader->values[1] - 1000) * 255.0d / 1000.0d, 0, 255);
  //int b = restrict((pwmReader->values[2] - 1000) * 255.0d / 1000.0d, 0, 255);
  int c = restrict((pwmReader->values[1] - 1000) * 255.0d / 1000.0d, 0, 255);

  if (toggler1->getCurrentState()) {
    pixels->setPixelColor(0, pixels->Color(10, c, 10));
    digitalWrite(PIN_MOTOR_STEP_0, HIGH);
    digitalWrite(PIN_MOTOR_STEP_1, HIGH);
    digitalWrite(PIN_MOTOR_STEP_2, HIGH);
    digitalWrite(PIN_MOTOR_STEP_3, HIGH);
  } else {
    pixels->setPixelColor(0, pixels->Color(c, 10, 10));
    digitalWrite(PIN_MOTOR_STEP_0, LOW);
    digitalWrite(PIN_MOTOR_STEP_1, LOW);
    digitalWrite(PIN_MOTOR_STEP_2, LOW);
    digitalWrite(PIN_MOTOR_STEP_3, LOW);
  }
  pixels->show();

}

