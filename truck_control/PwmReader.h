#ifndef pwm_reader_h
#define pwm_reader_h

#include "Arduino.h"
#include "Timer2MicroCounter.h"

class PwmReader {
  private:
    Timer2MicroCounter timer2MicroCounter;
    uint8_t port;
    uint8_t *pin_bits;
    int pin_count;
    int max_signal_length;

  public:
    volatile int *values;

    PwmReader(uint8_t port, uint8_t pwmPins[], int pin_count, int max_signal_length);
    ~PwmReader();
    void updateFromInterrupt();
};

#endif
