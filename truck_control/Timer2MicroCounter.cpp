#include <Arduino.h>

#include "Timer2MicroCounter.h"


Timer2MicroCounter::Timer2MicroCounter() {
  reset();
}

void Timer2MicroCounter::setup() {
  //increase the speed of timer2; see below link, as well as the datasheet pg 158-159.
  //Note: don't forget that when you speed up Timer2 like this you are also affecting any PWM output (using analogWrite) on Pins 3 & 11.  
  TCCR2B = TCCR2B & 0b11111000 | 0x02;

  //Enable Timer2 overflow interrupt; see datasheet pg. 159-160
  //TIMSK2 |= 0b00000001;
  
  //set timer2 to "normal" operation mode.  See datasheet pg. 147, 155, & 157-158 (incl. Table 18-8).  
  //-This is important so that the timer2 counter, TCNT2, counts only UP and not also down.
  //-To do this we must make WGM22, WGM21, & WGM20, within TCCR2A & TCCR2B, all have values of 0.
  TCCR2A &= 0b11111100;
  TCCR2B &= 0b11110111;

  reset();
}

void Timer2MicroCounter::reset() {
  overflow_count = 0;
  total_count = 0;
  TCNT2 = 0;
  TIFR2 |= 0b00000001;
}

unsigned long Timer2MicroCounter::micros() {
  return halfMicros() / 2; 
}

unsigned long Timer2MicroCounter::halfMicros() {
  uint8_t tcnt2_save = TCNT2; //grab the counter value from Timer2
  boolean flag_save = bitRead(TIFR2,0); //grab the timer2 overflow flag value; see datasheet pg. 160
  if (flag_save) { //if the overflow flag is set
    tcnt2_save = TCNT2; //update variable just saved since the overflow flag could have just tripped between previously saving the TCNT2 value and reading bit 0 of TIFR2.  
                        //If this is the case, TCNT2 might have just changed from 255 to 0, and so we need to grab the new value of TCNT2 to prevent an error of up 
                        //to 127.5us in any time obtained using the T2 counter (ex: T2_micros). (Note: 255 counts / 2 counts/us = 127.5us)
                        //Note: this line of code DID in fact fix the error just described, in which I periodically saw an error of ~127.5us in some values read in
                        //by some PWM read code I wrote.
    ++overflow_count;  //force the overflow count to increment
    TIFR2 |= 0b00000001; //reset Timer2 overflow flag since we just manually incremented above; see datasheet pg. 160; this prevents execution of Timer2's overflow ISR
  }
  total_count = overflow_count*256 + tcnt2_save;

  return total_count;
}

