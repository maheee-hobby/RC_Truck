#ifndef shift_register_writer_h
#define shift_register_writer_h

#include "Arduino.h"

class ShiftRegisterWriter {
  private:
    bool change;

    int ms1, ms2, ms3;
    int dir1, dir2, dir3, dir4;
    int sleep1, sleep2, sleep3, sleep4;

    uint8_t pinStrobe, pinData, pinClock;

  public:
    ShiftRegisterWriter(uint8_t pinStrobe, uint8_t pinData, uint8_t pinClock);

    void setMode(int ms1, int ms2, int ms3);
    void setDirection(int dir1, int dir2, int dir3, int dir4);
    void setSleep(int sleep1, int sleep2, int sleep3, int sleep4);

    void write();
    void forceWrite();
};

#endif
