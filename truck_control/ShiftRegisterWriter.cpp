#include "Arduino.h"

#include "ShiftRegisterWriter.h"

#include "IOUtil.h"


ShiftRegisterWriter::ShiftRegisterWriter(uint8_t pinStrobe, uint8_t pinData, uint8_t pinClock) {
  this->pinStrobe = pinStrobe;
  this->pinData = pinData;
  this->pinClock = pinClock;

  ms1 = 0;
  ms2 = 0;
  ms3 = 0;
  dir1 = 0;
  dir2 = 0;
  dir3 = 0;
  dir4 = 0;
  sleep1 = 1;
  sleep2 = 1;
  sleep3 = 1;
  sleep4 = 1;

  change = true;
}

void ShiftRegisterWriter::setMode(int ms1, int ms2, int ms3) {
  change = change || this->ms1 != ms1 || this->ms2 != ms2 || this->ms3 != ms3;
  this->ms1 = ms1;
  this->ms2 = ms2;
  this->ms3 = ms3;
}

void ShiftRegisterWriter::setDirection(int dir1, int dir2, int dir3, int dir4) {
  change = change || this->dir1 != dir1 || this->dir2 != dir2 || this->dir3 != dir3 || this->dir4 != dir4;
  this->dir1 = dir1;
  this->dir2 = dir2;
  this->dir3 = dir3;
  this->dir4 = dir4;
}

void ShiftRegisterWriter::setSleep(int sleep1, int sleep2, int sleep3, int sleep4) {
  change = change || this->sleep1 != sleep1 || this->sleep2 != sleep2 || this->sleep3 != sleep3 || this->sleep4 != sleep4;
  this->sleep1 = sleep1;
  this->sleep2 = sleep2;
  this->sleep3 = sleep3;
  this->sleep4 = sleep4;
}

void ShiftRegisterWriter::write() {
  if (change) {
    forceWrite();
  }
}

void ShiftRegisterWriter::forceWrite() {
  int data[] = {
    HIGH, ms3, ms2, ms1, HIGH, HIGH, HIGH, HIGH,
    dir1, dir2, dir3, dir4, sleep1, sleep2, sleep3, sleep4};
  int size = 16;

  uint8_t port_data  = digitalPinToPort(pinData);
  uint8_t bit_data   = digitalPinToBitMask(pinData);
  uint8_t port_clock = digitalPinToPort(pinClock);
  uint8_t bit_clock  = digitalPinToBitMask(pinClock);

  IOUtil::digitalWriteFast(pinStrobe, LOW); delayMicroseconds(5);

  for (int i = 0; i < size; ++i) {
    IOUtil::digitalWriteFast(port_data,  bit_data,  data[i]); delayMicroseconds(5);
    IOUtil::digitalWriteFast(port_clock, bit_clock, HIGH); delayMicroseconds(5);
    IOUtil::digitalWriteFast(port_clock, bit_clock, LOW);
  }

  delayMicroseconds(100);
  IOUtil::digitalWriteFast(pinStrobe, HIGH);

  change = false;
}
