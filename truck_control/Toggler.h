#ifndef Toggler_h
#define Toggler_h


class Toggler
{
  public:
    Toggler();
    Toggler(unsigned long lengthMillis);

    void reset();
    
    void setLengthMillis(unsigned long lengthMillis);
    bool getCurrentState();
    bool didToggle();

  private:
    volatile unsigned long lengthMillis;
    volatile unsigned long lastToggle;
    volatile bool state;
};

#endif
