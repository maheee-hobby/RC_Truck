#include "Arduino.h"

#include "PwmReader.h"


PwmReader::PwmReader(uint8_t port, uint8_t pwmPins[], int pin_count, int max_signal_length) {
  this->port = port;
  this->pin_count = pin_count;
  this->max_signal_length = max_signal_length * 2;

  this->pin_bits = new uint8_t[pin_count];
  this->values = new int[pin_count];

  for (int i = 0; i < pin_count; ++i) {
    pin_bits[i] = digitalPinToBitMask(pwmPins[i]);
  }

  timer2MicroCounter.setup();
}

PwmReader::~PwmReader() {
  delete[] this->pin_bits;
  delete[] this->values;
}

void PwmReader::updateFromInterrupt() {
  int pin_count = this->pin_count;
  int max_signal_length = this->max_signal_length;
  uint8_t port = this->port;

  //unsigned long start_time;
  unsigned long now;
  uint8_t portValue;
  int i;

  timer2MicroCounter.reset();
  //start_time = timer2MicroCounter.halfMicros();

  uint8_t pin_bits[pin_count];
  unsigned long end_times[pin_count];

  for (int i = 0; i < pin_count; ++i) {
    pin_bits[i] = this->pin_bits[i];
    end_times[i] = 0;
  }

  while (true) {
    now = timer2MicroCounter.halfMicros();
    portValue = *portInputRegister(port);

    // check pins and set end_time when pin is low
    for (i = 0; i < pin_count; ++i) {
      if (!(portValue & pin_bits[i]) && end_times[i] == 0) {
        end_times[i] = now;
      }
    }

    // check if we are done
    if (now > max_signal_length) {
      break;
    }
  }

  for (int i = 0; i < pin_count; ++i) {
    if (end_times[i] != 0) {
      //this->values[i] = (end_times[i] - start_time) / 2;
      this->values[i] = end_times[i] / 2;
    } else {
      this->values[i] = max_signal_length / 2;
    }
  }
}

