#include <Arduino.h>

#include "Toggler.h"


Toggler::Toggler() {
  this->reset();
}

Toggler::Toggler(unsigned long lengthMillis) {
  this->setLengthMillis(lengthMillis);
  this->reset();
}

void Toggler::reset() {
  this->lastToggle = millis();
  this->state = false;
}

void Toggler::setLengthMillis(unsigned long lengthMillis) {
  this->lengthMillis = lengthMillis;
}

bool Toggler::getCurrentState() {
  return this->state;
}

bool Toggler::didToggle() {
  unsigned long now = millis();
  if (this->lastToggle + this->lengthMillis < now) {
    this->lastToggle = now;
    this->state = !this->state;
    return true;
  }
  return false;
}

